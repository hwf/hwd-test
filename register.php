<?php

if (isset($_POST['login'])) {
    $login = $_POST['login'];
} else {
    $login = "";
}
if (isset($_POST['password'])) {
    $password = $_POST['password'];
} else {
    $password = "";
}
if (isset($_POST['gender'])) {
    $gender = $_POST['gender'];
} else {
    $gender = "";
}
// On verifie si des champs sont vides
if (empty($login) OR empty($password) OR empty($gender)) {
    header('Location: index?i=accounterror');
}
// Aucun champ n'est vide, on peut continuer
else {
    $isRegister = 1;
    try     {
        include($_SERVER['DOCUMENT_ROOT'] . '/include/db_access.php');
    }     catch (Exception $e)     {
        die('Erreur : ' . $e->getMessage());
    }
    $reponse = $bdd->prepare("SELECT mail FROM user WHERE mail=:login");
    $reponse->execute(array('login' => $login));
    while ($data = $reponse->fetch()) {
        if ($login == $data['mail']) {
            $isRegister = 0;
            header('Location: index?i=accountexist');
        }
    }
    $reponse->closeCursor();
    if ($isRegister != 0) {
        $reponse2 = $bdd->prepare("INSERT INTO user(id, state, mail, gender, password, birthdate, register_date) VALUES('', 0, :login, :gender, :password, '', now())");
        $reponse2->execute(array('login' => $login,'password' => password_hash($password, PASSWORD_DEFAULT),'gender' => $gender));
        $reponse2->closeCursor();
        $uid = sha1(uniqid('',true).'_'.mt_rand());
        $reponse3 = $bdd->prepare("INSERT INTO user_inscription(id, mail, token, date) VALUES('', :login, :token, now())");
        $reponse3->execute(array('login' => $login,'token' => $uid));
        $reponse3->closeCursor();
        
        $message = '<p>Bonjour,</p><p>Nous avons bien enregistré ton inscription, merci beaucoup !</p><p>Pour confirmer ton inscription, nous t\'invitons à cliquer sur le lien ci-dessous : </p><p><a href="http://'.$_SERVER['SERVER_NAME'].'/validate?uid='.$uid.'">http://'.$_SERVER['SERVER_NAME'].'/validate?uid='.$uid.'</a></p><p>Nous te souhaitons une belle journée.</p>';
        include($_SERVER['DOCUMENT_ROOT'].'/include/sendMail.php'); sendMail('html', 'HWF', $login,  'Inscription HWF', 'Bienvenue sur HWF', $message);
        header('Location: index?i=accountregistered');
    }
}
?>
