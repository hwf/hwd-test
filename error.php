<?php
//Random background
$randint = rand(1, 3);
//Error number
$error = $_GET['e'];
switch($error) {
    case '400':
        $number = "400";
        $message = "Bad Request!";
        break;
    case '401':
        $number = "401";
        $message = "Unauthorized!";
        break;
    case '402':
        $number = "402";
        $message = "Payment Required!";
        break;
    case '403':
        $number = "403";
        $message = "Forbidden!";
        break;
    case '404':
        $number = "404";
        $message = "File Not Found!";
        break;
    case '405':
        $number = "405";
        $message = "Method Not Allowed";
        break;
    case '500':
        $number = "500";
        $message = "Internal Server Error!";
        break;
    case '501':
        $number = "501";
        $message = "Not Implemented!";
        break;
    case '502':
        $number = "502";
        $message = "Bad Gateway!";
        break;
    case '503':
        $number = "503";
        $message = "Service Unavailable!";
        break;
    case '504':
        $number = "504";
        $message = "Gateway Time-Out!";
        break;
    case '505':
        $number = "505";
        $message = "HTTP Version Not Supported!";
        break;
    default:
        $number = "520";
        $message = "Unknown Error!";
        break;
}
?>
<!DOCTYPE html>
<html>
	<head>
		<style media="screen">
        @font-face {
            font-family: 'KeepCalm';
            src: url('/assets/fonts/KeepCalm.ttf') format('truetype');
        }
        body {
            background: url("/assets/images/BG_<?= $randint ?>.jpg");
            background-position: center center;
            background-repeat: no-repeat;
            background-attachment: fixed;
            background-size: cover;
            font-size: 100%;
        }
        .container_right {
            font-family: KeepCalm;
            height: 200px;
            margin-top: 4%;
            text-align: right;
            vertical-align: top;
            float: right;
            margin-right: 8%;
        }
        .num_1 {
            color: #e74c3c;
            font-size: 100px;
        }
        .num_2 {
            color: #f1c40f;
            font-size: 100px;
        }
        .num_3 {
            color: #2ecc71;
            font-size: 100px;
        }
        .message {
            color: #34495e;
            font-size: 2.2em;
            text-align: center;
        }
        a {
            text-decoration: none;
            color: #e67e22;
            transition: 0.5s;
        }
        a:hover {
            color: #d35400;
            transition: 0.5s;
        }
    </style>
		<meta charset="utf-8">
		<meta name="robots" content="noindex, nofollow">
		<link rel="shortcut icon" href="/assets/favicons/114.png">
		<title>Error: <?= $number ?></title>
	</head>
	<body>
		<div class="container_right">
			<span class="num_1"><?= $number[0] ?></span><span class="num_2"><?= $number[1] ?></span><span class="num_3"><?= $number[2] ?></span>
			<div class="message">Oops, <?= $message ?></div>
			<div><a href="/">Retour à l'accueil</a></div>
		</div>
	</body>
</html>
