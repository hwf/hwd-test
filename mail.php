<?php if (empty($_GET['title']) OR empty($_GET['message'])) {header('Location: home');} ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?= urldecode($_GET['title']) ?></title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <style media="screen">
            @import url(https://fonts.googleapis.com/css?family=Signika:700);
            body {
                background-color: whitesmoke;
            }
            .box_body {
                background-color: white;
                -webkit-box-shadow: 0 10px 6px -6px #777;
            	-moz-box-shadow: 0 10px 6px -6px #777;
            	box-shadow: 0 10px 6px -6px #777;
                width: 60%; margin-top: 20px;
                border-radius: 5px;
            }
            .box_body .nav_row {
                border-radius: 5px 5px 0 0;
                background: rgb(51, 102, 153);
                padding: 10px;
                min-height: 3em
            }
            .box_body .content_row {
                padding: 25px;
            }
            .box_body .content_row > div {
                margin-bottom: 50px;
            }
            .box_body .content_row h3 {
                font-family: Signika;
                font-size: 29px;
            }
            .box_body .footer_row {
                border-radius: 0 0 5px 5px;
                background: rgb(51, 102, 153);
                padding: 20px;
            }
            .twitter_hover {
                color: #55ACEE;
            }
            .twitter_hover:hover {
                color: #337ab7;
            }
        </style>
    </head>
    <body>
        <div class="container box_body">
            <div class="row nav_row"></div>
            <div class="row content_row">
                <div class="col-md-12 text-center">
                    <h3><?= urldecode($_GET['title']) ?></h3>
                </div>
                <div class="col-md-12 text-left">
                    <?= urldecode($_GET['message']) ?>
                </div>
                <div class="col-md-12 text-right">
                    L'&eacute;quipe BeeShary
                </div>
            </div>
            <div class="row footer_row">
                <div class="col-md-6 text-left">
                    Vous avez reçu ce mail car vous vous êtes inscrit sur HWF
                </div>
            </div>
        </div>
    </body>
</html>
