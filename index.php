<?php
include 'include/security.inc.php';
if (isset($_POST['login']) && isset($_POST['password']) && (check_auth($_POST['login'], $_POST['password'])))
{header('Location: /?i=loginsuccess');}
?>
<!DOCTYPE html>
<html>
<head>
	<title>HWF - Accueil</title>
</head>
<body>
	<h2>Coming soon...</h2>
        <form action="/register" method="post">
            <input type="text" name="login" placeholder="Adresse mail">
            <input type="password" name="password" placeholder="Mot de passe">
            <input type="password" name="password2" placeholder="Mot de passe (vérification)">
            <select id="gender" name="gender">
		<option selected>homme</option>
                <option>femme</option>
                <option>trans</option>
            </select>
            <div style="font-family: SignikaSemibold;">
                J'accepte les <b>conditions générales d'utilisation</b> et la <b>politique de confidentialité</b> de HWF
            </div>
            <input type="submit" name="submit" value="Inscription">
        </form>
        
        <form action="/index" method="post">
            <input type="text" name="login" placeholder="Adresse mail">
            <input type="password" name="password" placeholder="Mot de passe">
            <input type="submit" name="submit" value="Se connecter">
        </form>
        
        <button onclick="getLocation()">Afficher les données de géolocalisation</button>
        <p id="location"></p>

<script>
var x = document.getElementById("location");

function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else { 
        x.innerHTML = "Geolocation is not supported by your browser.";
    }
}

function showPosition(position) {
    x.innerHTML = "Latitude: " + position.coords.latitude + 
    "<br>Longitude: " + position.coords.longitude;	
}
</script>
</body>
</html>
