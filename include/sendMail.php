<?php

function sendMail($type, $from, $addr, $subject, $title, $message) {

// On verifie si des champs sont vides
if(empty($title) OR empty($message) OR empty($addr) OR empty($subject) OR empty($from))
{
	echo '<font color="red">Attention, aucun champs ne peut rester vide !</font>';
}

// Aucun champ n'est vide, on peut continuer
else
{
	// execution du script
        if (!preg_match("#^[a-z0-9._-]+@(hotmail|live|msn).[a-z]{2,4}$#", $addr)) {
            $line = "\r\n";
        }
        else {
            $line = "\n";
        }
        if ($type == "html") {
            $message = '<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Signika:700">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    </head>
    <style>
            @import url(https://fonts.googleapis.com/css?family=Signika:700);
            body {
                background-color: whitesmoke;
            }
            .box_body {
                background-color: white;
                -webkit-box-shadow: 0 10px 6px -6px #777;
            	-moz-box-shadow: 0 10px 6px -6px #777;
            	box-shadow: 0 10px 6px -6px #777;
                width: 60%; margin-top: 20px;
                border-radius: 5px;
            }
            .box_body .nav_row {
                border-radius: 5px 5px 0 0;
                background: rgb(250, 255, 80);
                padding: 10px;
                min-height: 3em;
            }
            .box_body .content_row {
                padding: 25px;
            }
            .box_body .content_row > div {
                margin-bottom: 50px;
            }
            .box_body .content_row h3 {
                font-family: Signika;
                font-size: 29px;
            }
            .box_body .footer_row {
                border-radius: 0 0 5px 5px;
                background: rgb(250, 255, 80);
                padding: 20px;
            }
            .twitter_hover {
                color: #55ACEE;
            }
            .twitter_hover:hover {
                color: #337ab7;
            }
            @media (max-width: 940px) {
            .box_body {
                background-color: white;
                -webkit-box-shadow: 0 10px 6px -6px #777;
            	-moz-box-shadow: 0 10px 6px -6px #777;
            	box-shadow: 0 10px 6px -6px #777;
                width: 100%; margin-top: 20px;
                border-radius: 5px;
            }
            }
        </style>
        <body style="background-color:#f5f5f5;">
        <div style="padding-right:15px;padding-left:15px;margin-right:auto;margin-left:auto;background-color: #f5f5f5;-webkit-box-shadow: 0 10px 6px -6px #777;-moz-box-shadow: 0 10px 6px -6px #777;box-shadow: 0 10px 6px -6px #777;width: 60%; margin-top: 20px;border-radius: 5px;">
            <div style="margin-right:-15px;margin-left:-15px;border-radius: 5px 5px 0 0;background: rgb(51, 102, 153);padding: 10px;min-height: 3em;">
                '.$mailhead.'
            </div>
            <div style="margin-right:-15px;margin-left:-15px;border-radius: 5px 5px 0 0;background: #fff;padding: 10px;min-height: 3em;">
                <div style="text-align:center;">
                    <h3 style="font-family: Signika;font-size: 20px;">'.$title.'</h3>
                </div>
                <div style="text-align:left;">
                    '.$message.'
                </div>
                <div style="text-align:right;">
                    L\'&eacute;quipe HWF
                </div>
            </div>
            <div style="margin-right:-15px;margin-left:-15px;border-radius: 0 0 5px 5px;background: rgb(51, 102, 153);padding: 20px;">
                <div style="text-align:left;">
                    Vous avez reçu ce mail car vous vous êtes inscrit sur HWF<br>
                    <a href="http://'.$_SERVER['SERVER_NAME'].'/mail?title='.urlencode($title).'&message='.htmlentities(urlencode($message)).'">Voir la version web</a>
                </div>
            </div>
        </div>
    </body>
</html>
';
            $end_text = '';
        } else {
            $end_text = '-----------------------------------------------------------'.$line.'Ceci est un message automatique, merci de ne pas y répondre'.$line.'-----------------------------------------------------------'.$line;
            $message = $message.$line.$line;
        }
        
        $headers ='From: '.utf8_decode($from).' <noreply@lithio.com>'.$line.
                  'Content-Type: text/'.$type.'; charset="UTF-8"'.$line.
                  'MIME-Version: 1.0'.$line.
                  'Content-Transfer-Encoding: 8bit'.$line.
                  'X-Sender: 5.196.124.139'.$line.
                  'X-Mailer: PHP/'.phpversion().$line;

        $message = $message.$end_text;
        mail($addr, $subject, $message, $headers);
}
}
?>
