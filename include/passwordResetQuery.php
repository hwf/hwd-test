<?php
if (isset($_POST['mail'])) {
    $mail = $_POST['mail'];
} else {
    $mail = "";
}
if (empty($mail)) {header('Location: /home');}
else {
    $isInDB = 1;
    try     {
        include($_SERVER['DOCUMENT_ROOT'] . '/include/db_access.php');
    }     catch (Exception $e)     {
        die('Erreur : ' . $e->getMessage());
    }
    $reponse = $bdd->prepare("SELECT mail, id, firstname FROM users WHERE mail=:mail");
    $reponse->execute(array('mail' => $mail));
    while ($data = $reponse->fetch()) {
        $isInDB = 0;
        $user_id = $data['id'];
        $firstname = $data['firstname'];
        $reponse2 = $bdd->prepare("SELECT user_id FROM password_reset WHERE user_id=:user_id");
        $reponse2->execute(array('user_id' => $user_id));
        while ($data = $reponse2->fetch()) {$isInDB = 1;echo $data['user_id'];}
        $reponse2->closeCursor();
    }
    $reponse->closeCursor();
    if ($isInDB == 0) {
        $uid = sha1(uniqid('',true).'_'.mt_rand());
        $reponse3 = $bdd->prepare("INSERT INTO password_reset(uid, user_id) VALUES(:uid, :user_id)");
        $reponse3->execute(array('uid' => $uid, 'user_id' => $user_id));
        $reponse3->closeCursor();
        $message = '<p>Bonjour '.$firstname.',</p><p>Vous avez envoyé une demande de réinitialisation de votre mot de passe.</p><p>Nous vous invitons à cliquer sur le lien ci-dessous et à changer votre mot de passe :</p><p><a href="http://'.$_SERVER['SERVER_NAME'].'/fr/passwordReset?uid='.$uid.'">http://'.$_SERVER['SERVER_NAME'].'/fr/passwordReset?uid='.$uid.'</a></p><p>Si vous n\'avez pas fait cette requête merci de ne pas tenir compte de ce mail.</p><p>Nous restons à votre disposition pour répondre à toutes vos questions.</p><p>Nous vous souhaitons de belles aventures et de jolies découvertes !</p><p>À bientôt.</p>';
        include($_SERVER['DOCUMENT_ROOT'].'/include/sendMail.php'); sendMail('html', 'BeeShary', $mail,  'Demande de réinitialisation de mot de passe', 'Réinitialiser votre mot de passe', $message, '');
    }
}
if ($isInDB == 1) {header("Location: /index?i=passwordqueryerror");}else {header("Location: /index?i=passwordqueryok");}
?>
