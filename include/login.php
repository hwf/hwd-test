<?php
	require_once('security.inc.php');

	function login() {
		$lang = $_GET['lang'];
		if(!isset($_POST['login']) || !isset($_POST['password'])) {
			header('Location: /'.$lang.'/login?e=1');
			return;
		}
		$isauth = check_auth($_POST['login'], $_POST['password']);
		if(!$isauth) {
				header('Location: /'.$lang.'/home?i=invalidaccount');
				return;
		}
		header('Location: /'.$lang.'/dashboard');
	};

	login();
?>
