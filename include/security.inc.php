<?php
    define('INACTIVITY_TIMEOUT', 7200); // in seconds
    ini_set('session.use_cookies', 1);       // Cookies to store session
    ini_set('session.use_only_cookies', 1);  // Force session's cookies (phpsessionID forbidden in URL)
    ini_set('session.use_trans_sid', false); // Prevent php to use sessionID in URL if cookies are disabled

    session_start();

    function allIPs() { // Client IP address (to prevent session cookie hijacking)
        $ip = $_SERVER["REMOTE_ADDR"];
        // HTTP headers to prevent session hijacking from users behind the same proxy
        if(isset($_SERVER['HTTP_X_FORWARDED_FOR'])) { $ip = $ip.'_'.$_SERVER['HTTP_X_FORWARDED_FOR']; }
        if(isset($_SERVER['HTTP_CLIENT_IP'])) { $ip = $ip.'_'.$_SERVER['HTTP_CLIENT_IP']; }
        return $ip;
    }

    function check_auth($login,$password) { // Check user/password
        try {
            include($_SERVER['DOCUMENT_ROOT'] . '/include/db_access.php');
        }
        catch (Exception $e) {
            die('Erreur : ' . $e->getMessage());
        }
        $reponse = $bdd->prepare("SELECT * FROM user WHERE mail=:login AND state=1");
	      $reponse->execute(array(':login' => $login));
        while($data = $reponse->fetch()) {
            if($data['mail'] == $login && password_verify($password, $data['password'])) {
                $_SESSION['uid'] = sha1(uniqid('',true).'_'.mt_rand()); // generate unique random number (different than phpsessionid) which can be used to hmac forms and form token (to prevent XSRF)
                $_SESSION['ip'] = allIPs();                		// Store IP address(es) of client to make sure session is not hijacked
                $_SESSION['expires_on'] = time()+INACTIVITY_TIMEOUT;  	// Set session expiration
                $_SESSION['user_id'] = $data['id'];
                $_SESSION['mail'] = $data['mail'];
                $_SESSION['gender'] = $data['gender'];
                $_SESSION['register_date'] = $data['register_date'];
                return true;
            }
            else {
                return false;
            }
        }
        $reponse->closeCursor();
    }

    function check_login() {  // Make sure user is logged in, else redirect to login page
        // If session does not exist on server or IP address has changed or session has expired => show login screen
        if(!isset ($_SESSION['uid']) || !$_SESSION['uid'] || $_SESSION['ip'] != allIPs() || time() >= $_SESSION['expires_on']) {
            logout();
        }
        $_SESSION['expires_on'] = time() + INACTIVITY_TIMEOUT;  // Update session expiration date when user accessed a page
    }

    function logout() {
        unset($_SESSION['uid'],$_SESSION['ip'],$_SESSION['expires_on']);   // Delete server session info
        header('Location: /?i=logout');
        exit();
        // We do not bother deleting the phpsessionID cookie, because it can't be used anyway (All server-side data attached to this session cookie is deleted. This makes the cookie useless)
    }
?>
