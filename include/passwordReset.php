<?php
if (isset($_POST['uid'])) {
    $uid = $_POST['uid'];
} else {
    $uid = "";
}
if (isset($_POST['password1'])) {
    $password1 = $_POST['password1'];
} else {
    $password1 = "";
}
if (isset($_POST['password2'])) {
    $password2 = $_POST['password2'];
} else {
    $password2 = "";
}
if (empty($uid) OR empty($password1) OR empty($password2)) {header('Location: /home');}
else {
    try     {
        include($_SERVER['DOCUMENT_ROOT'] . '/include/db_access.php');
    }     catch (Exception $e)     {
        die('Erreur : ' . $e->getMessage());
    }
    $reponse = $bdd->prepare("SELECT uid, user_id FROM password_reset WHERE uid=:uid");
    $reponse->execute(array('uid' => $uid));
    while ($data = $reponse->fetch()) {$user_id = $data['user_id'];}
    $reponse->closeCursor();
    if (isset($user_id)) {
        $reponse2 = $bdd->prepare("UPDATE users SET password=:password WHERE id=:user_id");
        $reponse2->execute(array('password' => password_hash($password1, PASSWORD_DEFAULT), 'user_id' => $user_id));
        $reponse2->closeCursor();
        
        $reponse3 = $bdd->prepare("DELETE FROM password_reset WHERE uid=:uid AND user_id=:user_id");
        $reponse3->execute(array('uid' => $uid, 'user_id' => $user_id));
        $reponse3->closeCursor();
    }
}
if (isset($user_id)) {header("Location: /index?i=passwordresetok");}else {header("Location: /index?i=passwordnoreset");}
?>
