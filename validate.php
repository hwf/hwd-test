<?php
if (isset($_GET['uid'])) {
    $uid = $_GET['uid'];
} else {
    $uid = "";
}
// On verifie si des champs sont vides
if (empty($uid)) {
    header('Location: index');
}
// Aucun champ n'est vide, on peut continuer
else {
    $isActivate = 0;
    try     {
        include($_SERVER['DOCUMENT_ROOT'] . '/include/db_access.php');
    }     catch (Exception $e)     {
        die('Erreur : ' . $e->getMessage());
    }
    $reponse = $bdd->prepare("SELECT mail FROM user_inscription WHERE token=:uid");
    $reponse->execute(array('uid' => $uid));

    while ($data = $reponse->fetch()) {
        $reponse2 = $bdd->prepare("UPDATE user SET state=1 WHERE mail=:user_mail");
        $reponse2->execute(array('user_mail' => $data['mail']));
        $reponse2->closeCursor();
        $reponse3 = $bdd->prepare("DELETE FROM user_inscription WHERE mail=:user_mail AND token=:uid");
        $reponse3->execute(array('user_mail' => $data['mail'],'uid' => $uid));
        $reponse3->closeCursor();
        $isActivate = 1;
    }
    $reponse->closeCursor();
}

if ($isActivate == 1) {header("Location: index?i=accountvalidate");
}else {header("Location: index?i=noaccountvalidate");}
?>
